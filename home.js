var pageTop = document.getElementById("page-top");
if (pageTop !== null) {
    // load html
    fetch('https://bitbucket.org/openexchange-nyse/openexchange-nyse-bc.bitbucket.io/raw/master/footer.html').then(function(response){
        return response.text();
    }).then(function(responseHTML){ // get html from url
        var footerContainer = document.querySelector(".footer-container")
        //put html in footerContainer 
        footerContainer.innerHTML = responseHTML;
    }).catch(function(err){
        // There was an error
        console.warn('Something went wrong.', err);
    });
}
